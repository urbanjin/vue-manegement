import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Welcome from '../components/Welcome.vue'
import UserList from '../components/admin/UserList.vue'
import CompanyList from '../components/CompanyList.vue'
import Sales from '../components/Sales.vue'
import Order from '../components/Order.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: "/",
    redirect: "/login"
  },
  {
    path: "/login",
    component: Login
  },
  {
    path: "/home",
    component: Home,
    redirect: "/welcome",
    children: [
      { path: "/welcome", component: Welcome, },
      { path: "/user", component: UserList, },
      { path: "/company", component: CompanyList, },
      { path: "/salelist", component: Sales, },
      { path: "/orderlist", component: Order, },
    ]
  },
]

const router = new VueRouter({
  routes
})

// 出现问题的时候使用
const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(location, onResolve, onReject) {
  if (onResolve || onReject) return originalPush.call(this, location, onResolve, onReject)
  return originalPush.call(this, location).catch(err => err)
}
//挂载路由导航守卫
router.beforeEach((to,from,next) => {
  if(to.path =='/login') return next();
  const userFlag = window.sessionStorage.getItem("user");
  if(!userFlag) return next('/login');
  next();
})

export default router
